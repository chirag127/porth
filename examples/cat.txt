:i argc 3
:b arg0 20
./examples/cat.porth
:b arg1 3
foo
:b arg2 28
./examples/hello-world.porth
:b stdin 0

:i returncode 0
:b stdout 938
include "std.porth"

memory fd sizeof(u64) end
proc @fd -- int in fd @64 end
proc !fd int in fd !64 end

const BUFFER_CAP 1024 end
memory buffer BUFFER_CAP end

memory file_path_cstr sizeof(ptr) end
proc @file_path_cstr -- ptr in file_path_cstr @64 cast(ptr) end
proc !file_path_cstr ptr in file_path_cstr !64 end

proc cat_fd in
  BUFFER_CAP buffer @fd read
    while dup 0 > do
       buffer puts
       BUFFER_CAP buffer @fd read
    end
  drop
end

proc main in
  argc 2 < if
    stdin !fd
    cat_fd
  else
    1 while dup argc < do
      dup nth_argv !file_path_cstr
  
      0 O_RDONLY @file_path_cstr AT_FDCWD openat
  
      dup 0 < if
        "ERROR: could not open file " eputs
        @file_path_cstr cstr-to-str eputs
        "\n" eputs
        drop
      else
        fd !64
        cat_fd
        fd @64 close drop
      end
  
      1 +
    end drop
  end
end
include "std.porth"

proc main in
  "Hello, World\n" puts
end

:b stderr 31
ERROR: could not open file foo

